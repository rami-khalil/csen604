package exceptions;

public class InvalidPageException extends Exception {

	private static final long serialVersionUID = 4506979190934619454L;

	/**
	 * @param message Message for the exception constructor.
	 */
	public InvalidPageException(String message) {
		super(message);
	}
}
