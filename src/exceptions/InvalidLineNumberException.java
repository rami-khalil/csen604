package exceptions;

public class InvalidLineNumberException extends Exception {

	private static final long serialVersionUID = -6185225561857860195L;

	/**
	 * @param message Message for the exception constructor.
	 */
	public InvalidLineNumberException(String message) {
		super(message);
	}
}
