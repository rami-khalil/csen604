package exceptions;


public class InvalidTypeException extends Exception {

	private static final long serialVersionUID = -4234360820540151616L;

	public InvalidTypeException(String message) {
		super(message);
	}
	
}
