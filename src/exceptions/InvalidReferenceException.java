package exceptions;

public class InvalidReferenceException extends Exception {

	private static final long serialVersionUID = -1264879105064411122L;

	public InvalidReferenceException(String message) {
		super(message);
	}
}
