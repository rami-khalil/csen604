package exceptions;


public class DBAppException extends Exception {
	private static final long serialVersionUID = -4375634600637550573L;
	
	public DBAppException() {
		super();
	}
	public DBAppException(String message) {
		super(message);
	}

}
