package exceptions;

public class InvalidTableNameException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4609615879679873000L;

	/**
	 * @param message Message for the exception constructor.
	 */
	public InvalidTableNameException(String message) {
		super(message);
	}
}
