package bptree;
import java.io.Serializable;

/* B+ Tree data structure.
 * Authored by Rami Khalil.
 * Assumes Key extends Comparable.
 * Assumes non-null values.
 * Assumes no duplicates.
 * Assumes branchingFactor >= 4
 * 
 * Do not copy or use without permission.
 * */

@SuppressWarnings("unchecked")
public class BPTree<Key extends Comparable<Key>, Value> implements Serializable {
	private static final long serialVersionUID = 7530952110728381252L;

	public final class Node implements Serializable {
		private static final long serialVersionUID = -4080744397056177509L;
		
		private int m;
		private boolean leaf;
		private Key[] keys = (Key[]) new Comparable[branchingFactor];
		private Object[] values = new Object[branchingFactor];
		private Node next, prev, parent;
		
		public Node(boolean leaf) {
			this.leaf = leaf;
		}
		
		public boolean isFull() {
			return m == maxChildren();
		}
		
		public Node split() {
			// Split node
			Node neu = new Node(leaf);					
			for(int i = m/2 + m%2; i < m; i++) {
				neu.insert(keys[i], values[i]);
				keys[i] = null;
				values[i] = null;
			}
			m -= neu.m;
			if(!leaf)
				keys[m-1] = null;
			// Fix pointers
			neu.parent = parent;
			neu.next = next;
			neu.prev = this;
			next = neu;
			return neu;
		}
		
		// Insert value into leaf.
		public void insert(Key key, Object value) {
			if(isFull())
				return;

			int i;
			keys[m] = key;
			values[m] = value;

			if(!leaf) {
				Node ptr = (Node) value;
				ptr.parent = this;
			}
			
			m++;
			if(key == null) {
				return;
			}
		
			
			for(i = m-2; i >= 0; i--) {
				if(less(keys[i], key))
					break;
				
				keys[i+1] = keys[i];
				values[i+1] = values[i];
			}

			keys[i+1] = key;
			values[i+1] = value;
		}
		
		// Insert value into non-leaf
		public void insertDual(Key key, Object lvalue, Object rvalue) {
			if(isFull() || key == null)
				return;
			
			int i;
			for(i = m-1; i >= 0; i--) {
				if(less(keys[i], key))
					break;

				keys[i+1] = keys[i];
				values[i+1] = values[i];
			}
						
			keys[i+1] = key;
			values[i+1] = lvalue;
//			System.out.println("Duality insertion: " + key + " after " + i + " in " + this);
			values[i+2] = rvalue;
			
			// Fix pointers
			if(!leaf) {
				Node ptr = (Node) lvalue;
				ptr.parent = this;
				ptr = (Node) rvalue;
				ptr.parent = this;
			}
			m++;
		}

		public int minChildren() {
			return leaf ? branchingFactor/2 : branchingFactor/2 + branchingFactor%2;
		}
		
		public int maxChildren() {
			return leaf ? branchingFactor - 1 : branchingFactor;
		}
		
		public boolean atleastHalfFull() {
			return m >= minChildren();
		}
				
		public boolean aboveHalfFull() {
			return m > minChildren();
		}
		
		public boolean isSiblingOf(Node target) {
			return target != null && target.parent == parent;
		}
	
		// Delete value from node
		public void delete(Key key) {
			boolean shift = false;
			for(int i = 0; i < m; i++) {
				if(equal(key, keys[i]))
					shift = true;
				
				if(shift) {
					keys[i] = keys[i+1];
					values[i] = values[i+1];
				}
			}
			m--;
		}
	
		// target >> this
		public void shiftRightFrom(Node target) {
			if(target.m == 0)
				return;
			
			for(int i = m; i > 0; i--) {
				keys[i] = keys[i-1];
				values[i] = values[i-1];
			}
						
			m++;
			target.m--;

			keys[0] = target.keys[target.m];
			values[0] = target.values[target.m];

			target.keys[target.m] = null;
			target.values[target.m] = null;
			
			if(!leaf) {
				// New parent
				Node neu = (Node) values[0];
				neu.parent = this;
				// Cut old ties
				if(neu.prev != null) {
					neu.prev.next = null;
					neu.prev = null;
				}
				// New connection
				neu.next = (Node) values[1];
				neu.next.prev = neu;
				// New median
				updateMedians(this);
//				keys[0] = neu.next.keys[0];
			}
		}
		
		// this << target
		public void shiftLeftFrom(Node target) {
			if(target.m == 0)
				return;
			
			keys[m] = target.keys[0];
			values[m]= target.values[0];
			
			m++;
			target.m--;

			for(int i = 0; i < target.m; i++) {
				target.keys[i] = target.keys[i+1];
				target.values[i] = target.values[i+1];
			}
			target.keys[target.m] = null;
			target.values[target.m] = null;
			
			if(!leaf) {
				// New parent
				Node neu = (Node) values[m-1];
				neu.parent = this;
				// Cut old ties
				if(neu.next != null) {
					neu.next.prev = null;
					neu.next = null;
				}
				// New connection
				neu.prev = (Node) values[m-2];
				neu.prev.next = neu;
				// New median
//				System.out.print("After shift: " + this + " ");
//				for(int i = 0; i < m; i++)
//					System.out.print("(" + keys[i] +", " + values[i] +") ");
//				System.out.println();
				
				updateMedians(this);
//				keys[m-2] = neu.keys[0];
			}
		}
	
		public int getPos() {
			for(int i = 0; parent != null && i < parent.m; i++)
				if(this == parent.values[i])
					return i;
			return -1;
		}
	
		public void defrag(int pos) {
//			System.out.println(this + " defrag " + pos);
			boolean shift = false;
			
			for(int i = 0; i < m-1; i++) {
				if(i == pos)
					shift = true;
				
				if(shift) {
					keys[i] = keys[i+1];
					values[i] = values[i+1];
				}
			}
			
			if(shift || pos == m-1) {
				keys[m-1] = null;
				values[m-1] = null;
				m--;
			}
		}
	}
	
	private int branchingFactor, height, count;
	private Node root;
	
	public BPTree(int branchingFactor) {
		this.branchingFactor = branchingFactor; // >= 4
	}
	
	public boolean contains(Key key) {
		Node target = search(key, root);
		for(int i = 0; target != null && i < target.m; i++)
			if(equal(key, target.keys[i]))
				return true;
		return false;
	}
	
	public Value search(Key key) {
		Node target = search(key, root);
		if(target != null)
			for(int i = 0; i < target.m; i++)
				if(equal(key, target.keys[i]))
					return (Value) target.values[i];
		return null;
	}
	
	private Node search(Key key, Node node) {
		if(node == null)
			return null;
		else if(node.leaf)
			return node;
		
		for(int i = 0; i < node.m-1; i++)
			if(less(key, node.keys[i]))
				return search(key, (Node) node.values[i]);
		
		return search(key, (Node) node.values[node.m-1]);
	}
	
	public Value lower_bound(Key key) {
		return lower_bound(key, root);
	}
	
	private Value lower_bound(Key key, Node node) {
		if(node == null)
			return null;
		else if(node.leaf) {
			for(int i = 0; i < node.m; i++)
				if(!less(key, node.keys[i]))
						return (Value) node.values[i];
			return null;
		}
		
		for(int i = 0; i < node.m-1; i++)
			if(less(key, node.keys[i])) {
				Value maybe = lower_bound(key, (Node) node.values[i]);
				if(maybe != null)
					return maybe;
				return lower_bound(key, (Node) node.values[i+1]); // called at most once per query.
			} else if(equal(key, node.keys[i]))
				return lower_bound(key, (Node) node.values[i+1]);
		
		return lower_bound(key, (Node) node.values[node.m-1]);
	}
	
	public void insert(Key key, Value value) {
		if(contains(key)) {
			//update(key, value);
			return;
		}
		count++;
		
		if(root == null) {
			root = new Node(true);
			root.insert(key,  value);
		} else {
			Node target = search(key, root);
//			System.out.println(key + "---->" + target);
			if(target.isFull()) {
				Node neu = target.split();
				if(less(key, target.keys[target.m-1])) {
					target.insert(key, value);
				} else {
					neu.insert(key, value);
				}
				passUp(minKeyInTree(neu), target, neu, neu.parent);
			} else {
				target.insert(key, value);
			}
		}
	}
	
	private void passUp(Key key, Node left, Node right, Node node) {
//		System.out.println("Passup: " + key + " " + left + " " + right + " " + node);
		if(node == null) { // New root
			root = new Node(false);
			root.insert(key, left);
			root.insert(null, right);
			left.parent = root;
			right.parent = root;
			height++;
		} else { // attempt insertion
			if(node.isFull()) {
				// split node
				Node sibling = node.split(), neu = new Node(false), parent = node.parent;
				node.parent = neu;
				sibling.parent = neu;
				
				if(less(key, node.keys[node.m-2])) {
//					System.out.println("TAKE CARE!");
					sibling.shiftRightFrom(node);
					node.insertDual(key, left, right);
				} else
					sibling.insertDual(key, left, right);
				
				passUp(minKeyInTree(sibling), node, sibling, parent);
				updateMedians(parent);
			} else {
				node.insertDual(key, left, right);
			}
		}
	}
	
	private Key minKeyInTree(Node root) {
		if(root == null)
			return null;
		else if(root.leaf)
			return root.keys[0];
		
		return minKeyInTree((Node) root.values[0]);
	}
		
	public void update(Key key, Value value) {
		if(!contains(key))
			return;
		Node target = search(key, root);
		for(int i = 0; target != null && i < target.m; i++)
			if(equal(key, target.keys[i]))
				target.values[i] = value;
	}

	public void delete(Key key) {
		Node target = search(key, root);
		if(target == null)
			return;
		
//		System.out.println("Let's do it @ " + target);
		target.delete(key);
		count--;
		
		if(target != root && !target.atleastHalfFull())
			rebalance(target);
		else if(target == root && target.m == 0)
			root = null;
		
		updateMedians(target);
	}
	
	private void rebalance(Node node) {
//		System.out.println("Rebalance: " + node);
		if(node.atleastHalfFull())
			return;
		
		if(node.isSiblingOf(node.prev) && node.prev.aboveHalfFull()) {
			node.shiftRightFrom(node.prev);
		} else if(node.isSiblingOf(node.next) && node.next.aboveHalfFull()) {
			node.shiftLeftFrom(node.next);
		} else if(node.isSiblingOf(node.next)) {
			// merge
			while(node.next.m > 0)
				node.shiftLeftFrom(node.next);
			// delete empty node
			node.parent.defrag(node.next.getPos());
			// fix pointers
			node.next = node.next.next;
			if(node.next != null)
				node.next.prev = node;
			// update median keys
			updateMedians(node.parent);
//			System.out.println("DBG:");
//			print();
			// balance parent
			rebalance(node.parent);
		} else if(node.isSiblingOf(node.prev)) {
			// merge
			while(node.prev.m > 0)
				node.shiftRightFrom(node.prev);
			// delete empty node
			node.parent.defrag(node.prev.getPos());
			// fix pointers
			node.prev = node.prev.prev;
			if(node.prev != null)
				node.prev.next = node;
			// update median keys
			updateMedians(node.parent);
			// balance parent
			rebalance(node.parent);
		} else {
//			System.out.println("This case?" + root.leaf + " " + root.m);
			if(!root.leaf && root.m < 2) {
				root = (Node) root.values[0];
				height--;
			}
		}
	}
	
	private void updateMedians(Node node) { // O(b*logb(n)^2)
		if(node == null)
			return;
		
		if(!node.leaf) {
			for(int i = 0; i < node.m-1; i++)
				node.keys[i] = minKeyInTree((Node) node.values[i+1]);
			node.keys[node.m-1] = null;
		}
		
		updateMedians(node.parent);
	}

	@SuppressWarnings("rawtypes")
	private boolean equal(Comparable a, Comparable b) {
		return a != null && b != null && a.equals(b);
	}
	
	@SuppressWarnings("rawtypes")
	private boolean less(Comparable a, Comparable b) {
		return a != null && b != null && a.compareTo(b) < 0;
	}
	
	/*
	private boolean lessEq(Comparable a, Comparable b) {
		return a != null && b != null && a.compareTo(b) <= 0;
	}
	*/
	
	/*
	private void print() {
		
		if(root == null) {
			System.out.println("Empty tree.");
		} else {
			Object Q[] = new Object[500];
			int Qi[] = new int[500];
			
			int i = 0, j = 1;
			Q[0] = root;
			Qi[0] = 0;
			while(i != j) {
				System.out.println(Q[i] + " at level: " + Qi[i]);
				Node n = (Node) Q[i];
				for(int k = 0; k < n.m; k++) {
					System.out.print("(" + (Key) n.keys[k] + ',' + (Value) n.values[k] + ") ");
					if(!n.leaf) {
						Q[j] = n.values[k];
						Qi[j++] = Qi[i] + 1;
					}
				}
				System.out.println();
				i++;
			}
		}
	}
	*/
	
	/*
	public static void main(String[] args) {
		//testing
		BPTree<Integer, Integer> test = new BPTree<Integer, Integer>(5);
		int arr[] = new int[]{ 5, 9, 4, 3, 1, 6, 7, 8, 2, 12, 11, 10 };
//		for(int i = 0; i < 12; i++) {
//			test.insert(arr[i], arr[i]);
//			System.out.println("Passed: " + arr[i]);
//			test.print();
//			System.out.println("\n\n\n\n\n\n\n\n\n\n");
//		}
		
		
		
		int lim = 1000000;
		long startTime = System.nanoTime();

		
		for(int i = 0; i < lim/2; i++)
			test.insert(2*i, 2*i);
		for(int i = 0; i < lim/2; i++)
			test.insert(2*i+1, 2*i+1);		
//		test.print();
				
		for(int i = 0; i < lim; i++) {
			test.delete(i);
		}
		
		long endTime = System.nanoTime(), duration = endTime - startTime;
		duration /= 1e6;
		System.out.println("Inserted & Deleted " + lim + " elements in ~" + duration + "ms");

	}
	*/
		
}
