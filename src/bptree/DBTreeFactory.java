package bptree;

import java.util.Properties;

import utilities.Row;

public class DBTreeFactory {
	
	/**
	 * @param properties - The DBApp properties.
	 * @return a B+Tree supporting duplicates.
	 */
	public static BPTreeDup<String, Row> getDuplicateTree(Properties properties) {
		return 	new BPTreeDup<String, Row>(
				Integer.parseInt(
						properties.getProperty("BPlusTreeN")
						)
				);
	}
	
	/**
	 * @param properties - The DBApp properties.
	 * @return a B+Tree not supporting duplicates.
	 */
	public static BPTree<String, Row> getNonDuplicateTree(Properties properties) {
		return 	new BPTree<String, Row>(
				Integer.parseInt(
						properties.getProperty("BPlusTreeN")
						)
				);
	}
}
