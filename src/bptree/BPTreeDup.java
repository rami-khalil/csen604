package bptree;

import java.io.Serializable;
import java.util.ArrayList;

public class BPTreeDup<Key extends Comparable<Key>, Value> implements Serializable {
	private static final long serialVersionUID = 7057205422942024113L;
	BPTree<Key, ArrayList<Value>> tree;
	
	public BPTreeDup(int factor) {
		tree = new BPTree<Key, ArrayList<Value>>(factor);
	}
	
	public int count(Key key) {
		return tree.contains(key) ? tree.search(key).size() : 0;
	}
	
	public ArrayList<Value> search(Key key) {
		ArrayList<Value> result = tree.search(key);
		if(result == null)
			result = new ArrayList<Value>();
		return result;
	}
	
	public ArrayList<Value> lower_bound(Key key) {
		return tree.lower_bound(key);
	}
	
	public void insert(Key key, Value value) {
		ArrayList<Value> values = tree.search(key);
		if(values == null) {
			values = new ArrayList<Value>();
			tree.insert(key, values);
		}
		values.add(value);
	}
	
	public void delete(Key key) {
		tree.delete(key);
	}
	
	public void delete(Key key, Value value) {
		ArrayList<Value> target = search(key);
		if(target != null) {
			target.remove(value);
			if(target.size() == 0)
				delete(key);
		}
	}
}
