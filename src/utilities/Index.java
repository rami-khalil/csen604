package utilities;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Properties;

import bptree.BPTreeDup;
import bptree.DBTreeFactory;

public class Index implements Serializable {
	private static final long serialVersionUID = -3481372159501701004L;
	private BPTreeDup<String, Row> indexTree;
	
	/**
	 * Constructs a new index on a column. 
	 * @param targetColumn - The target column.
	 * @param properties - The target DBApp properties.
	 */
	public Index(Column targetColumn, Properties properties) {
		indexTree = DBTreeFactory.getDuplicateTree(properties);
	}
	

	/**
	 * @param indexValue - The target search value.
	 * @return A set of rows indexed by the target indexValue.
	 */
	public Iterator<Row> search(String indexValue) {
		System.out.println(indexValue + indexTree.search(indexValue));
		return indexTree.search(indexValue).iterator();
	}
	
	/**
	 * @param indexValue - The target search value.
	 * @return - The number of entries in the index matching the values.
	 */
	public int count(String indexValue) {
		return indexTree.count(indexValue);
	}
	
	/**
	 * @param inserted - The row to be inserted in the index.
	 */
	public void insert(String key, Row inserted) {
		indexTree.insert(key, inserted);
	}
	
	/**
	 * @param deleted - The row to be deleted from the index.
	 */
	public void delete(String key, Row deleted) {
		Iterator<Row> iter = search(key);
		while(iter.hasNext()) {
			Row r = iter.next();
			if(r.getPageNumber() == deleted.getPageNumber() && r.getLineNumber() == deleted.getLineNumber()) {
				iter.remove();
				break;
			}
		}
//		indexTree.delete(key, deleted);
	}
	
}
