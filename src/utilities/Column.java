package utilities;

import java.io.Serializable;
import java.util.HashMap;

import metadatahandlers.MetadataEntry;

public class Column implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -196193058106953786L;
	private Table table;
	private String title;
	private boolean isIndex;
	private boolean isKey;
	private String reference;
	private Index index;
	private String type;

	/**
	 * Constructs a new column object for the target table.
	 * @param table - The target table.
	 * @param title - The name of the column
	 * @param type - The data type in the column.
	 * @param isIndex - True IFF the column is an index, false otherwise.
	 * @param isKey - True IFF the column is the key, false otherwise.
	 * @param reference - The referenced column. Format: TableName.ColumnName
	 */
	public Column(Table table, String title, String type, boolean isIndex,
			boolean isKey, String reference) {
		this.table = table;
		this.title = title;
		this.isIndex = isIndex;
		this.isKey = isKey;
		this.reference = reference;
	}

	/**
	 * @return The name of the column
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the name of the column.
	 * @param title - The new name.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return True IFF the column is an index, false otherwise.
	 */
	public boolean isIndex() {
		return isIndex;
	}

	/**
	 * Sets the index status of the column.
	 * @param isIndex - The new index status.
	 */
	public void setIndex(boolean isIndex) {
		this.isIndex = isIndex;
	}

	/**
	 * @return False unless the column is the key.
	 */
	public boolean isKey() {
		return isKey;
	}

	/**
	 * Sets the key status of the column. 
	 * @param key - The new key status.
	 */
	public void setKey(boolean key) {
		this.isKey = key;
	}

	/**
	 * @return The referenced column. Format: TableName.ColumnName
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * Sets the new referenced column.
	 * @param reference - The new referenced column: Format: TableName.columnName
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @param tableMap
	 * @return The column object referenced by this column object's reference string.
	 */
	public Column getColumnReference(HashMap<String, Table> tableMap) {
		String refTokens[] = PageManager.splitOnDot(getReference());
		if(refTokens.length != 2)
			return null;
		
		String targetTableName = refTokens[0];
		String targetColumnName = refTokens[1];
		Table targetTable = tableMap.get(targetTableName);
		if(targetTable == null)
			return null;

		Column targetColumn = targetTable.getColumn(targetColumnName);
		if(targetColumn == null || !targetColumn.isKey())
			return null;
		
		return targetColumn;
	}
	
	/**
	 * @return - This column's table object.
	 */
	public Table getTable() {
		return table;
	}

	/**
	 * @param table - The new table object this column belongs to.
	 */
	public void setTable(Table table) {
		this.table = table;
	}

	/**
	 * @return - This column's index object.
	 */
	public Index getIndex() {
		return index;
	}

	/**
	 * @param index - This column's new index object.
	 */
	public void setIndex(Index index) {
		this.index = index;
	}

	/**
	 * @return - This column's data type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type - This column's new data type.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return A metadata entry representation of this column's state.
	 */
	public MetadataEntry getMetadata() {
		return new MetadataEntry(table.getName(), getTitle(), getType(),
				isKey(), isIndex(), getReference());
	}
}
