package utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import exceptions.InvalidReferenceException;

public class Table {
	private String name;
	private ArrayList<Column> columns;

	/**
	 * Constructor.
	 * @param name - The new table's name.
	 */
	public Table(String name) {
		this.name = name;
		this.columns = new ArrayList<Column>();
	}

	/**
	 * Adds the column objects in to this table's list. 
	 * @param tableMap - The target DBApp's table map. 
	 * @param htblColNameType - The set of column/type pairs.
	 * @param htblColNameRefs - The set of column/name pairs.
	 * @param strKeyColName - The key column name.
	 */
	public void addColumns(HashMap<String, Table> tableMap,
			Hashtable<String, String> htblColNameType,
			Hashtable<String, String> htblColNameRefs, String strKeyColName) {
		//TODO: Handle null values.
		Set<String> keys = htblColNameType.keySet();

		for (String key : keys) {
			String title = key;
			boolean isIndex = false;
			boolean isKey = false;

			if (title.equals(strKeyColName))
				isKey = isIndex = true;

			String reference = htblColNameRefs.get(key);
			String[] refTableCol;
			
			
			if (reference.contains("."))
				 refTableCol = PageManager.splitOnDot(reference);
			else {
				refTableCol = new String[1];
				refTableCol[0] = reference;
			}
			
			if (refTableCol.length < 2)
				try {
					throw new InvalidReferenceException(
							"Cannot resolve as reference.");
				} catch (InvalidReferenceException e1) {
					e1.printStackTrace();
				}
			if (refTableCol[0].equals(this.name))
				try {
					throw new InvalidReferenceException(
							"Cannot reference same table.");
				} catch (InvalidReferenceException e1) {
					e1.printStackTrace();
				}

			if (!tableMap.containsKey(refTableCol[0]) && refTableCol.length > 1)
				try {
					throw new InvalidReferenceException("Cannot find table: " + refTableCol[0]);
				} catch (InvalidReferenceException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			String type = htblColNameType.get(key);

			/*
			if (!TypeValidator.isValidType(type))
				try {
					throw new InvalidTypeException("Cannot resolve" + type
							+ " to a type.");
				} catch (InvalidTypeException e) {
					e.printStackTrace();
				}
			*/

			Column col = new Column(this, title, type, isIndex, isKey,
					reference);
			addColumn(col);

		}

	}


	/**
	 * @return This table's name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name This table's new name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param column - The column to be added.
	 */
	public void addColumn(Column column) {
		columns.add(column);
	}

	/**
	 * @param column - The column name to be fetched.
	 * @return The target column object.
	 */
	public Column getColumn(String column) {
		for (Column c : columns)
			if (c.getTitle().equals(column))
				return c;
		return null;
	}

	/**
	 * @param column - Search column
	 * @return False unless target column is in this table's list.
	 */
	public boolean containsColumn(String column) {
		return columns.contains(getColumn(column));
	}
	
	/**
	 * @return An iterator to this table's list of columns.
	 */
	public Iterator<Column> getColumns() {
		return columns.iterator();
	}

}
