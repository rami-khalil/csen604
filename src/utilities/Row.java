package utilities;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import exceptions.InvalidPageException;
import exceptions.InvalidTableNameException;

public class Row implements Serializable{
	private static final long serialVersionUID = 5560111478798350819L;
	// There should be a variable containing the Row data
	private int pageNumber, lineNumber;
	private String tableName;

	/**
	 * Constructs a new Row pointer object for the target table. 
	 * @param table - The target table.
	 * @param pageNumber - The referenced page number.
	 * @param lineNumber - The referenced line number.
	 */
	public Row(String tableName, int pageNumber, int lineNumber) {
		this.setTableName(tableName);
		this.pageNumber = pageNumber;
		this.lineNumber = lineNumber;
	}
					
	/**
	 * @return This row's page number.
	 */
	public int getPageNumber() {
		return pageNumber;
	}
	
	/**
	 * @return This row's line number.
	 */
	public int getLineNumber() {
		return lineNumber;
	}
	
	/**
	 * @return ArrayList of strings containing this row's page data.
	 */
	public ArrayList<String> getRowPage() {
		ArrayList<String> page;
		try {
			page = PageManager.getPage(tableName, pageNumber);
		} catch (InvalidTableNameException e) {
			e.printStackTrace();
			return null;
		} catch (InvalidPageException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return page;
	}
	
	/**
	 * @param columnName - Target column
	 * @return Target column's stored value on disk for this row
	 */
	public String getValue(String columnName) {
		return getAllValues().get(columnName);
	}
	
	/**
	 * @return A hashmap of all column/value pairs of this row.
	 */
	public HashMap<String, String> getAllValues() {
		ArrayList<String> page = getRowPage();
		if(page.size() < lineNumber)
			return null;

		Iterator<String> iter = page.iterator();
		String fetched = "";
		for(int i = 0; i < lineNumber; i++) fetched = iter.next();

		return parseDataString(fetched);
	}
	
	public static HashMap<String, String> parseDataString(String data) {
		HashMap<String, String> result = new HashMap<String, String>();
		String []split = data.split(", ");
		for(int i = 0; i < split.length; i++) {
			String []dataSplit = split[i].split(":");
			String first = dataSplit[0];
			String second = dataSplit[1];
			for(int j = 2; j < dataSplit.length; j++)
				second += dataSplit[j];
			result.put(first, second);
		}
		return result;
	}

	/**
	 * @return This row's table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName This row's new table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public boolean equals(Row other) {
		return this.tableName.equals(other.tableName) && this.pageNumber == other.pageNumber && this.lineNumber == other.lineNumber;
	}
}
