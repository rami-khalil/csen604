package utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;

import dbengine.FileHandler;
import exceptions.InvalidLineNumberException;
import exceptions.InvalidPageException;
import exceptions.InvalidTableNameException;

public class PageManager {

	private static BufferedReader br;

	/**
	 * Creates the first page file for the target table.
	 * @param tableName - Target table.
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static void createPage(String tableName) {
		int num = getPageCount(tableName);
		try {
			PrintWriter wr = new PrintWriter(FileHandler.pagesPath + tableName
					+ "/" + num + ".csv", "UTF-8");
			wr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param tableName - The target table name.
	 * @return - The number of page files for the table.
	 * @throws InvalidTableNameException
	 */
	public static int getPageCount(String tableName) {
			return FileHandler.countFiles(FileHandler.pagesPath + tableName + "/");
	}

	/**
	 * @param tableName - The target table name.
	 * @param pageNum - The taget page number.
	 * @return A list of lines in the target page.
	 * @throws InvalidTableNameException
	 * @throws InvalidPageException
	 * @throws IOException
	 */
	public static ArrayList<String> getPage(String tableName, int pageNum)
			throws InvalidTableNameException, InvalidPageException, IOException {

		if (getPageCount(tableName) < pageNum+1) {
			throw new InvalidPageException("Cannot find page #" + pageNum
					+ " in " + tableName);
		}

		br = new BufferedReader(new FileReader(FileHandler.pagesPath + tableName + "/"
				+ pageNum + ".csv"));

		String line;
		ArrayList<String> lines = new ArrayList<String>();

		while ((line = br.readLine()) != null)
			lines.add(line);

		return lines;

	}

	/**
	 * @param tableName - The target table name.
	 * @param pageNum - The targe page number.
	 * @param lineNum - The taget line number.
	 * @return - The contents of the target line in the target page of the target table.
	 * @throws InvalidLineNumberException
	 */
	public static String getLine(String tableName, int pageNum, int lineNum)
			throws InvalidLineNumberException {
		ArrayList<String> lines = new ArrayList<String>();
		try {
			lines = getPage(tableName, pageNum);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (lines.size() < lineNum)
			throw new InvalidLineNumberException("Cannot find data at line #"
					+ lineNum + " in pages/" + tableName + "/" + pageNum
					+ ".csv");

		return lines.get(lineNum);
	}

	/**
	 * @param tableName - The target table name.
	 * @param pageNum - The target page number.
	 * @return - The number of lines in the target page.
	 */
	public static int getLineNumbers(String tableName, int pageNum) {
		FileReader reader = null;
		try {
			String pathToFile = FileHandler.pagesPath + tableName + "/" + pageNum + ".csv";
			reader = new FileReader(pathToFile);
		} catch (IOException e) {
			System.out.println("Error reading file.");
			return 0;
		}
		Scanner scanner = new Scanner(reader);
		int cnt = 0;
		while (scanner.hasNextLine()) {
//			String line = scanner.nextLine();
			scanner.nextLine();
			cnt++;
		}
		scanner.close();
		return cnt;
	}

	/**
	 * Inserts data into the page file of the target table.
	 * @param tableName - The target table name.
	 * @param pageNum - The targe page file number.
	 * @param data - The data to tbe inserted.
	 * @throws Exception
	 */
	public static void insertLine(String tableName, int pageNum, String data)
			{
		int maxLines = getLineNumbers(tableName, pageNum);
		
		try {
			insertLineNumber(tableName, pageNum, maxLines + 1, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Inserts data into the target line in the target page file of the target table.
	 * @param tableName - The target table.
	 * @param pageNum - The target page number.
	 * @param lineNum - The target line number.
	 * @param data - The data to be inserted.
	 * @throws Exception
	 */
	public static void insertLineNumber(String tableName, int pageNum,
			int lineNum, String data) throws Exception {
//		int maxLines = getLineNumbers(tableName, pageNum);
//		if (lineNum <= maxLines + 1) {
			FileReader reader = null;
			FileWriter writer = null;

			String pathToFile = FileHandler.pagesPath + tableName + "/" + pageNum + ".csv";
			String tmpPath = FileHandler.pagesPath + ".tmp.csv";

			reader = new FileReader(pathToFile);
			writer = new FileWriter(tmpPath);

			Scanner scanner = new Scanner(reader);
			BufferedWriter bw = new BufferedWriter(writer);

			int cnt = 0;
			boolean written = false;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (cnt == lineNum) {
					bw.write(data);
					written = true;
				} else {
					bw.write(line);
				}
				bw.write("\n");
				cnt++;
			}
			if(!written)
				bw.write(data + "\n");

			bw.flush();
			bw.close();
			scanner.close();

			reader = new FileReader(tmpPath);
			writer = new FileWriter(pathToFile);

			scanner = new Scanner(reader);
			bw = new BufferedWriter(writer);

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				bw.write(line);
				bw.write("\n");
			}

			bw.flush();
			bw.close();
			scanner.close();
						
			return;
//		}
	}

	/**
	 * 
	 * @param s - String to be splitted
	 * @return - Array of strings splitted by a dot
	 */
	public static String[] splitOnDot(String s) {
		String now = "";
		String[] ret = new String[2];
		int cnt = 0;
		for (int i=0; i<s.length(); i++) {
			char c = s.charAt(i);
			if (c == '.') {
				ret[cnt] = now;
				cnt ++;
				now = "";
				continue;
			}
			now += c;
		}
		ret[cnt] = now;
		return ret;
	}
}
