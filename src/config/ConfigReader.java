package config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import dbengine.FileHandler;

public class ConfigReader {
	private FileReader reader;
	
	/**
	 * Constructs a new instance of the configuration reader.
	 */
	public ConfigReader() {
		try {
			setReader(new FileReader(FileHandler.configPath));
		} catch(FileNotFoundException e) {
			System.out.println("(ConfigReader:ConfigReader)Critical Error: Could not find config file.");
			return;
		}
	}
	
	
	/**
	 * Reads from the configuration file into the target properties object.
	 * @param properties - The target properties object.
	 * @return - True if successful, false otherwise.
	 */
	public boolean read(Properties properties) {
		try {
			properties.load(new FileInputStream(FileHandler.configPath));
		} catch (IOException e) {
			System.out.println("(ConfigReader:read)Critical Error: Could not load config file.");
			return false;
		}
		return true;
	}

	/**
	 * @return The config file reader.
	 */
	public FileReader getReader() {
		return reader;
	}

	/**
	 * @param reader The new config file reader.
	 */
	public void setReader(FileReader reader) {
		this.reader = reader;
	}

}
