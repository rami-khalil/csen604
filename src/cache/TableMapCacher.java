package cache;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import utilities.Column;
import utilities.Table;

public class TableMapCacher {
	
	/**
	 * Cache all the indices of the target table map.
	 * @param tableMap - The table map.
	 */
	public static void cache(HashMap<String, Table> tableMap) {
		Iterator<Map.Entry<String, Table>> it = tableMap.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, Table> tablePair = it.next();
			Iterator<Column> jt = tablePair.getValue().getColumns();
			while(jt.hasNext()) {
				Column target = jt.next();
				if(target.isIndex())
					DBCacher.save(target);
			}
		}
	}
}
