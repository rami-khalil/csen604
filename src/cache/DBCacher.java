package cache;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Properties;

import utilities.Column;
import utilities.Index;
import utilities.Table;
import dbengine.FileHandler;
import dbengine.IndexCreator;
import exceptions.DBAppException;

public class DBCacher {	
	
	/**
	 * Load a cached index into the target column 
	 * @param column - The target column.
	 * @throws DBAppException 
	 */
	public static boolean load(Properties properties,
			HashMap<String, Table> tableMap, Column column) throws DBAppException {
		String completeName = column.getTable().getName() + ":" + column.getTitle();
		try {
			String path = FileHandler.cachePath + column.getTable().getName() + "/" + column.getTitle() + FileHandler.cacheFileSuffix;
			FileInputStream indexCacheFile = new FileInputStream(path);
			ObjectInputStream indexCacheIn = new ObjectInputStream(indexCacheFile);
			Index loaded = (Index) indexCacheIn.readObject();
			if(loaded == null) {
				IndexCreator.createIndex(properties, tableMap, column.getTable().getName(),
						column.getTitle());
			} else {
				column.setIndex(loaded);
			}
			indexCacheIn.close();
			indexCacheFile.close();
			
			System.out.println("(DBCacher:load) " + completeName + " Index cache loaded.");
			return true;
		} catch (IOException e) {
			System.out.println("(DBCacher:load) " + completeName + " Index cache not found.");
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.out.println("(DBCacher:load) " + completeName + " Index cache could not be loaded.");
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Cache the index of a column to disk.
	 * @param column - The column to cache.
	 */
	public static void save(Column column) {
		String completeName = column.getTable().getName() + ":" + column.getTitle();
		try {
			String path = FileHandler.cachePath + column.getTable().getName();
			FileHandler.mkdir(path);
			path += "/" + column.getTitle() + FileHandler.cacheFileSuffix;
			FileOutputStream indexCacheFile = new FileOutputStream(path);
			ObjectOutputStream indexCacheOut = new ObjectOutputStream(indexCacheFile);
			indexCacheOut.writeObject(column.getIndex());
			indexCacheOut.flush();
			indexCacheOut.close();
			indexCacheFile.close();
		} catch (IOException e) {
			System.out.println("(DBCacher:load) " + completeName + " Error saving index file.");
			e.printStackTrace();
		}
	}
	
}
