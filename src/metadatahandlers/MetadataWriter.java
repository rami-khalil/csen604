package metadatahandlers;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MetadataWriter {

	static final String METADATA_FILE_NAME = "data/metadata.csv";
	static PrintWriter out;

	/**
	 * @param meta - Metadata to be written into the metadata file.
	 * @throws IOException
	 */
	public static void write(MetadataEntry meta) throws IOException {
		out = new PrintWriter(new BufferedWriter(new FileWriter(
				METADATA_FILE_NAME, true)));
		out.println(meta.toString());
		out.close();
	}
}
