package metadatahandlers;

public class MetadataEntry {
	private String table, column, type, reference;
	private boolean key, index;
	
	/**
	 * Constructs a new meta data entry.
	 * @param table - Table name
	 * @param column - Column name
	 * @param type - Type name
	 * @param key - key status
	 * @param index - index status
	 * @param reference - referenced column
	 */
	public MetadataEntry(String table, String column, String type, boolean key, boolean index, String reference) {
		this.table = table;
		this.column = column;
		this.type = type;
		this.key = key;
		this.index = index;
		this.reference = reference;
	}
	
	/**
	 * Construct a new metadata entry object from a string.
	 * @param entry - Metadata string.
	 */
	public MetadataEntry(String entry) {
		String tokens[] = entry.split(", ");
		table = tokens[0];
		column = tokens[1];
		type = tokens[2];
		key = tokens[3].toLowerCase().equals("true");
		index = tokens[4].toLowerCase().equals("true");
		reference = tokens[5];
	}
	
	/**
	 * Empty constructor.
	 */
	public MetadataEntry() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return Stored table.
	 */
	public synchronized String getTable() {
		return table;
	}

	/**
	 * @param table New stored table.
	 */
	public synchronized void setTable(String table) {
		this.table = table;
	}

	/**
	 * @return Stored column
	 */
	public synchronized String getColumn() {
		return column;
	}

	/**
	 * @param column New stored column.
	 */
	public synchronized void setColumn(String column) {
		this.column = column;
	}

	/**
	 * @return Stored type.
	 */
	public synchronized String getType() {
		return type;
	}

	/**
	 * @param type New stored type.
	 */
	public synchronized void setType(String type) {
		this.type = type;
	}

	/**
	 * @return Stored reference.
	 */
	public synchronized String getReference() {
		return reference;
	}

	/**
	 * @param reference New stored reference
	 */
	public synchronized void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return Key status
	 */
	public synchronized boolean isKey() {
		return key;
	}

	/**
	 * @param key new keystatus
	 */
	public synchronized void setKey(boolean key) {
		this.key = key;
	}

	/**
	 * @return Index status
	 */
	public synchronized boolean isIndex() {
		return index;
	}

	/**
]	 * @param index new index status
	 */
	public synchronized void setIndex(boolean index) {
		this.index = index;
	}

	/**
	 * @return string representation of this metadata object
	 */
	public synchronized String toString() {
		String keyString = (key ? "True" : "False"), indexString = (index ? "True" : "False");
		
		return table + ", " + column + ", " + type + ", " + keyString + ", " + indexString + ", " + reference;
	}
}
