package metadatahandlers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;

import utilities.Column;
import utilities.Table;
import cache.DBCacher;
import dbengine.FileHandler;
import dbengine.IndexCreator;
import exceptions.DBAppException;

public class MetadataReader {
	private FileReader reader;
	private FileWriter writer;
	
	/**
	 * Constructor.
	 */
	public MetadataReader() {
		try {
			reader = new FileReader(FileHandler.metadataPath);
			reader.close();
		} catch(FileNotFoundException e) {
			try {
				writer = new FileWriter(FileHandler.metadataPath);
				writer.write("");
				writer.flush();
				writer.close();
			} catch (IOException e1) {
				System.out.println("(MetadataReader:MetadataReader) Critical error: Could not find a metadata file or create a new one.");
			}
			System.out.println("(MetadataReader:MetadataReader) Fresh metadata file created.");
		} catch(IOException e) {
			// Unkown error.
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads the contents of the metadata files into the target table map
	 * @param tableMap - The target table map.
	 * @return False unless operation completed successfully.
	 */
	public boolean read(Properties properties, HashMap<String, Table> tableMap) {
		try {
			reader = new FileReader(FileHandler.metadataPath);
		} catch (IOException e) {
			System.out.println("(MetadataReader:read) Error reading metadata file.");
			return false;
		}
		Scanner scanner = new Scanner(reader);
		while(scanner.hasNextLine()) {
			String line = scanner.nextLine();
			MetadataEntry metadata = new MetadataEntry(line);
			
			Table table;
			// If table does not exist, create it
			if(!tableMap.containsKey(metadata.getTable())) {
				table = new Table(metadata.getTable());
				tableMap.put(metadata.getTable(), table);
			} else {
				table = tableMap.get(metadata.getTable());
			}
			// Add column to table
			Column column = new Column(table, metadata.getColumn(), metadata.getType(), metadata.isIndex(), 
					metadata.isKey(), metadata.getReference());
			table.addColumn(column);
			// Load cached index
			if(column.isIndex()) {
				try {
					if(!DBCacher.load(properties, tableMap, column))
						try {
							IndexCreator.createIndex(properties, tableMap, table.getName(), column.getTitle());
						} catch (DBAppException e) {
							e.printStackTrace();
						}
				} catch (DBAppException e) {
					e.printStackTrace();
					scanner.close();
					return false;
				}
			}
		}
		scanner.close();
		return true;
	}
}
