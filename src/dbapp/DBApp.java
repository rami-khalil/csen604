package dbapp;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import utilities.Table;
import cache.TableMapCacher;
import dbengine.IndexCreator;
import dbengine.Initializer;
import dbengine.RowDeleter;
import dbengine.RowInserter;
import dbengine.RowSelector;
import dbengine.TableCreator;
import exceptions.DBAppException;
import exceptions.InvalidReferenceException;

public class DBApp {
	private Properties properties;
	private HashMap<String, Table> tableMap;
	private boolean initialized;

	/**
	 * Creates an initialized DBApp instance.
	 */
	public DBApp() {
		this(true);
	}

	/**
	 * Creates a DBApp instance.
	 * @param initialize - True: Initialize the DBApp. False: Wait for DBApp.init() to be called.
	 */
	public DBApp(boolean initialize) {
		initialized = false;
		if (initialize)
			init();
	}

	/**
	 * Initialize the DBApp Instance.
	 * Loads configuration and metadata files.
	 */
	public void init() {
		if (initialized)
			return;

		properties = new Properties();
		tableMap = new HashMap<String, Table>();

		initialized = Initializer.initializeDB(properties, tableMap);
	}

	/**
	 * Creates a new table.
	 * @param strTableName - The table name.
	 * @param htblColNameType - A map of column types.
	 * @param htblColNameRefs - A map of column references.
	 * @param strKeyColName - The name of the key column.
	 * @throws DBAppException - Thrown if table exists or key not found.
	 * @throws InvalidReferenceException 
	 */
	public void createTable(String strTableName,
			Hashtable<String, String> htblColNameType,
			Hashtable<String, String> htblColNameRefs, String strKeyColName)
			throws DBAppException {
		isInitalized();

		TableCreator.createTable(properties, tableMap, strTableName,
				htblColNameType, htblColNameRefs, strKeyColName);
	}

	/**
	 * Creates an index on the target column in the target table.
	 * @param strTableName - The target table.
	 * @param strColName - The target column.
	 * @throws DBAppException - Thrown if table/column does not exist.
	 */
	public void createIndex(String strTableName, String strColName)
			throws DBAppException {
		isInitalized();

		IndexCreator
				.createIndex(properties, tableMap, strTableName, strColName);
	}
	
	
	/**
	 * Inserts a row into the target table.
	 * @param strTableName - The target table.
	 * @param htblColNameValue - A map of of column/value pairs.
	 * @throws DBAppException - Thrown if table does not exist or key column value missing.
	 */
	public void insertIntoTable(String strTableName,
			Hashtable<String, String> htblColNameValue) throws DBAppException {
		isInitalized();

		RowInserter.insertRow(properties, tableMap, strTableName,
				htblColNameValue);
	}

	/**
	 * Deletes zero or values from the target table.
	 * @param strTableName - Target table.
	 * @param htblColNameValue - Conditions for deletion.
	 * @param strOperator - Operator on conditions.
	 * @throws DBAppException - Thrown if table does not exist/column in condition does not exist.
	 */
	public void deleteFromTable(String strTableName,
			Hashtable<String, String> htblColNameValue, String strOperator)
			throws DBAppException {
		isInitalized();

		RowDeleter.deleteRow(properties, tableMap, strTableName,
				htblColNameValue, strOperator);
	}

	/**
	 * Selects a set of values that match the search criteria from a target table.
	 * @param strTable - The target table.
	 * @param htblColNameValue - A map of column/value pairs for the search.
	 * @param strOperator - Operator for search.
	 * @return An iterator to the set of results.
	 * @throws DBAppException - Thrown if table does not exist/column does not exist in table.
	 */
	public Iterator selectFromTable(String strTable,
			Hashtable<String, String> htblColNameValue, String strOperator)
			throws DBAppException {
		isInitalized();

		return RowSelector.selectFromTable(properties, tableMap, strTable,
				htblColNameValue, strOperator);
	}

	/**
	 * Saves all the indices in memory to disk.
	 * @throws DBAppException - Thrown on caching error.
	 */
	public void saveAll() throws DBAppException {
		isInitalized();
		TableMapCacher.cache(tableMap); // Cache indices for future reference.
	}

	/**
	 * 
	 * @throws DBAppException
	 */
	private void isInitalized() throws DBAppException {
		if (!initialized)
			throw new DBAppException(
					"The DBApp was not correctly intialized. Tried calling init()?");
	}
}
