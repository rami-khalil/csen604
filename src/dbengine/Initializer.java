package dbengine;

import java.util.HashMap;
import java.util.Properties;

import metadatahandlers.MetadataReader;
import config.ConfigReader;
import utilities.Table;

public class Initializer {
	
	/**
	 * Initialize the DBApp Instance.
	 * Loads configuration and metadata files.
	 * @param properties - The target DBApp properties.
	 * @param tableMap - The target DBApp tableMap.
	 * @return true iff initialization successful, false otherwise.
	 */
	public static boolean initializeDB(Properties properties, HashMap<String, Table> tableMap) {
		// Read properties file
		ConfigReader configuration = new ConfigReader();
		if(!configuration.read(properties))
			return false;
		System.out.println("Config: " + properties);
		
		// Read metadata file
		MetadataReader reader = new MetadataReader();
		if(!reader.read(properties, tableMap))
			return false;

		return true;
	}
}
