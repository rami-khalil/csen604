package dbengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Stack;

import utilities.Column;
import utilities.PageManager;
import utilities.Row;
import utilities.Table;
import exceptions.DBAppException;

public class RowDeleter {

	/**
	 * Deletes zero or values from the target table.
	 * @param properties - The target DBApp properties.
	 * @param tableMap - The target DBApp tableMap.
	 * @param strTableName - Target table.
	 * @param htblColNameValue - Conditions for deletion.
	 * @param strOperator - Operator on conditions.
	 * @throws DBAppException - Thrown if table does not exist/column in condition does not exist.
	 */
	public static void deleteRow(Properties properties,
					HashMap<String, Table> tableMap, String strTableName,
					Hashtable<String, String> htblColNameValue, String strOperator)
					throws DBAppException {
		
		Table t = tableMap.get(strTableName);
		Iterator<Row> r = RowSelector.selectFromTable(properties, tableMap, strTableName, htblColNameValue, strOperator);
		while(r.hasNext()) {
			Row row = r.next();
			markRow(strTableName, row);
			updateBPTree(t, row);
			cascade(tableMap, strTableName, row);
		}
	}
	
	public static void markRow(String strTableName, Row row) {
		try {
			String data = PageManager.getLine(strTableName, row.getPageNumber(), row.getLineNumber());
			System.out.println("Marking (" + row.getLineNumber() + ": " + data);
			data = "*" + data;
			PageManager.insertLineNumber(strTableName, row.getPageNumber(), row.getLineNumber(), data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void cascade(HashMap<String, Table> tableMap, String strTableName, Row pro) {
		Stack<Row> s = new Stack<Row>();
		
		s.push(pro);
		
		while (!s.empty()) {
			try {
				Row now = s.pop();
				Table t = tableMap.get(now.getTableName());
				Iterator<Column> it = t.getColumns();
				ArrayList<Column> cols = new ArrayList<Column>();
				while (it.hasNext()) {
					cols.add(it.next());
				}
				String data = PageManager.getLine(t.getName(), now.getPageNumber(), now.getLineNumber());
				data = data.substring(1);
				String[] parts = data.split(", ");
				for (int i=0; i<parts.length; i++) {
					String[] colV = parts[i].split(":");
					String colName = colV[0];
					String colVal = colV[1];
					for (int j=0; j<cols.size(); j++) {
						Column cur = cols.get(j);
						if (colName.equals(cur.getTitle())) {
							if (cur.isKey()) {
								ArrayList<Row> results = findAll(tableMap, t.getName(), colName, colVal, "");
								for (int k=0; k<results.size(); k++) {
									Row h = results.get(k);
									markRow(t.getName(), h);
									s.push(h);
								}
							}
							break;
						}
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static ArrayList<Row> findAll(HashMap<String, Table> tableMap, String tableName,
			String colName, String colVal, String strOperator) throws DBAppException {
		
		Set<String> keys = tableMap.keySet();
		ArrayList<Row> res = new ArrayList<Row>();
		ArrayList<String> resTables = new ArrayList<String>();
		for (String key : keys) {
			Table t = tableMap.get(key);
			Iterator<Column> it = t.getColumns();
			
			while (it.hasNext()) {
				Column col = it.next();
				String ref = col.getReference();

				if (!ref.equals("null")) {
					String[] p = PageManager.splitOnDot(ref);
					if (p[0].equals(tableName) && p[1].equals(colName)) {
						String newName = col.getTitle();
						String value =  colVal;
						Hashtable<String, String> ht = new Hashtable<String, String>();
						ht.put(newName, value);
						Iterator<Row> m = RowSelector.selectFromTable(null, tableMap, t.getName(), ht, strOperator);
						while (m.hasNext()) {
							Row l = m.next();
							if (!checkExistingRow(res, resTables, t.getName(), l)) {
								res.add(l);
								resTables.add(t.getName());
							}
						}
					}
				}
			}
		}
		return res;
	}

	private static boolean checkExistingRow(ArrayList<Row> res, ArrayList<String> resTables, String strTableName, Row l) {
		for (int i=0; i<res.size(); i++) {
			Row n = res.get(i);
			String m = resTables.get(i);
			if (m.equals(strTableName)) {
				if (n.getLineNumber() == l.getLineNumber() && n.getPageNumber() == l.getPageNumber()) {
					return false;
				}
			}
		}
		return true;
	}

	public static void updateBPTree(Table t, Row row) {
		Iterator<Column> c = t.getColumns();
		while (c.hasNext()) {
			Column tmp = c.next();
			if (tmp.isIndex()) {
				tmp.getIndex().delete(row.getValue(tmp.getTitle()), row);
			}
		}
	}

}
