package dbengine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import utilities.Column;
import utilities.Index;
import utilities.PageManager;
import utilities.Row;
import utilities.Table;
import exceptions.DBAppException;
import exceptions.InvalidPageException;
import exceptions.InvalidTableNameException;

public class IndexCreator {

	/**
	 * Creates an index on the target column in the target table.
	 * @param properties - The target DBApp properties.
	 * @param tableMap - The target DBApp tableMap.
	 * @param strTableName - The target table.
	 * @param strColName - The target column.
	 * @throws DBAppException - Thrown if table/column does not exist.
	 */
	public static void createIndex(Properties properties,
			HashMap<String, Table> tableMap, String strTableName,
			String strColName) throws DBAppException {
//		System.out.println("Creating fresh index.");

		if (!tableMap.containsKey(strTableName))
			throw new DBAppException("(IndexCreator:createIndex) Error: Table "
					+ strTableName + " does not exist.");

		Table table = tableMap.get(strTableName);
		Column column = table.getColumn(strColName);
		if (column == null)
			throw new DBAppException(
					"(IndexCreator:createIndex) Error: Column " + strColName
							+ " does not exist in " + strTableName + ".");

		Index idx = new Index(column, properties);
		column.setIndex(true);
		column.setIndex(idx);
		
//		System.out.println("Created: " + column.getIndex());
		int pageCount = PageManager.getPageCount(strTableName);
		for(int i = 0; i < pageCount; i++) {
			try {
				ArrayList<String> data = PageManager.getPage(column.getTable().getName(), i);
//				System.out.println("Page " + i + ": " + data.size());
				Iterator<String> dataIterator = data.iterator();
				int j = 0;
				while(dataIterator.hasNext()) {
					String rowData = dataIterator.next();
					if(rowData.charAt(0) == '*') {
						System.out.println("Deleted already: " + rowData);
						j++;
						continue;
					}
					HashMap<String, String> colValues = Row.parseDataString(rowData);
					Row inserted = new Row(strTableName, i, j);
//					System.out.println(strColName +": " + colValues.get(strColName));
					idx.insert(colValues.get(strColName), inserted);
					j++;
				}
			} catch (InvalidTableNameException e) {
				break;
			} catch (InvalidPageException e) {
				break;
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}

}
