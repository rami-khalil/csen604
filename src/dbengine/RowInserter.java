package dbengine;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import utilities.Column;
import utilities.Index;
import utilities.PageManager;
import utilities.Row;
import utilities.Table;
import exceptions.DBAppException;
import exceptions.InvalidTableNameException;

public class RowInserter {

	/**
	 * Inserts a row into the target table.
	 * @param properties - The target DBApp properties.
	 * @param tableMap - The target DBApp tableMap.
	 * @param strTableName - The target table.
	 * @param htblColNameValue - A map of of column/value pairs.
	 * @throws DBAppException - Thrown if table does not exist or key column value missing.
	 * @throws InvalidTableNameException 
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public static void insertRow(Properties properties,
			HashMap<String, Table> tableMap, String strTableName,
			Hashtable<String, String> htblColNameValue) throws DBAppException {
		
		
		if (!tableMap.containsKey(strTableName))
			throw new DBAppException(
					"Unable to Find The Table, try creating the table first");
		
		Table table = tableMap.get(strTableName);
		String data = "";

		ArrayList<Column> referenced_columns = new ArrayList<>();
		Column Key_column = null;

		// Get array list of referenced column objects
		// Get Key Column if any
		Iterator<Column> col = table.getColumns();
		while (col.hasNext()) {
			Column c = col.next();
			if (c.getColumnReference(tableMap) != null) {
				referenced_columns.add(c);
			}
			
			if (c.isKey()) {
				Key_column = c;
			}
		}

		col = referenced_columns.iterator();
		while(col.hasNext()) {
			Column column = col.next();
			String colName = column.getTitle();
			String value = htblColNameValue.get(colName);
			if (checkForeignKeys(properties, tableMap, column, value)) 
				throw new DBAppException(String.format("The Entered Foreign key '%s' does not exist in its Table", value));
		}
		
		if(Key_column != null) {
			String value = htblColNameValue.get(Key_column.getTitle());
			if (checkKeys(properties, tableMap,strTableName, Key_column, value))
				throw new DBAppException(String.format("The Entered Record Primary key '%s' is already exist in the Table",value));
		}

		Table t = tableMap.get(strTableName);
		Set<String> Keys = htblColNameValue.keySet();
		for (String key : Keys) {
			
			if(!t.containsColumn(key))
				throw new DBAppException(String.format("The target table %s does not contain the column %s.", 
						strTableName, key));
			
			String value = htblColNameValue.get(key);
			if (!data.isEmpty())
				data += ", ";
			
			data += String.format("%s:%s", key,value);
		}
		
		
		int pageNo = PageManager.getPageCount(strTableName);
		if(pageNo == 0) {
			PageManager.createPage(strTableName);
			pageNo++;
		}
		
		int rowNo = PageManager.getLineNumbers(strTableName, pageNo-1);
				
		if(rowNo == Integer.parseInt((String) properties.get("MaximumRowsCountinPage"))) {
			PageManager.createPage(strTableName);
			pageNo++;
		}
		
		try {
			PageManager.insertLine(strTableName, pageNo-1, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Row data_inserted = new Row(strTableName, pageNo, rowNo);

		col = table.getColumns();
		while (col.hasNext()) {
			Column c = (Column) col.next();
			if (c.isIndex()) {
				Index index = c.getIndex();
				index.insert(htblColNameValue.get(c.getTitle()), data_inserted);
				c.setIndex(index);
			}
		}

	}

	private static boolean checkForeignKeys(Properties properties,HashMap<String, Table> tableMap,
			Column c, String colValue) {
		if (c != null) {
			String[] tableAndColName = PageManager.splitOnDot(c.getReference());
			
			Hashtable<String, String> ColNameValue = new Hashtable<>();
			ColNameValue.put(tableAndColName[1], colValue);
			
			try {
				Iterator<Row> checkedTable = RowSelector.selectFromTable(properties,tableMap, tableAndColName[0], ColNameValue, "");
				while (checkedTable.hasNext()) {
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
	
	private static boolean checkKeys(Properties properties,HashMap<String, Table> tableMap,
			String strTableName,
			Column Key_column,
			String colValue){

		String colName = Key_column.getTitle();
		if (Key_column != null) {
			Hashtable<String, String> ColNameValue = new Hashtable<>();
			ColNameValue.put(colName, colValue);
			try{
				Iterator<Row> checkedTable = RowSelector.selectFromTable(
						properties, tableMap,
						strTableName, ColNameValue, "");
				while (checkedTable.hasNext()) {
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

}
