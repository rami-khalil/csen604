package dbengine;

import java.io.File;

public class FileHandler {
	public static final String cachePath = "data/cache/";
	public static final String cacheFileSuffix = ".cache";

	public static final String configPath = "config/DBApp.config";
	public static final String metadataPath = "data/metadata.csv";
	
	public static final String pagesPath = "data/pages/";

	/**
	 * Recursively create any missing folders in the target path.
	 * @param path - The target path
	 * @return true if and only if the directory was created; false otherwise
	 */
	public static boolean mkdir(String path) {
		File target = new File(path);
		return target.mkdir();
	}
	
	/**
	 * Lists the files and directories in a target path.
	 * @param path - Target path
	 * @return An array of strings naming the files and directories in the directory denoted by this abstract pathname. The array will be empty if the directory is empty. Returns null if this abstract pathname does not denote a directory, or if an I/O error occurs.
	 */
	public static String[] ls(String path) {
		mkdir(path);
		return new File(path).list();
	}
	
	/**
	 * Count the number of files/folders in a target path.
	 * @param path - The target path.
	 * @return - Number of files/folders.
	 */
	public static int countFiles(String path) {
		mkdir(path);
		return new File(path).list().length;
	}
}
