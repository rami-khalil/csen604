package dbengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import exceptions.DBAppException;
import exceptions.InvalidLineNumberException;
import utilities.Column;
import utilities.Index;
import utilities.PageManager;
import utilities.Row;
import utilities.Table;

public class RowSelector {

	/**
	 * Selects a set of values that match the search criteria from a target table.
	 * @param properties - The target DBApp properties.
	 * @param tableMap - The target DBApp tableMap.
	 * @param strTable - The target table.
	 * @param htblColNameValue - A map of column/value pairs for the search.
	 * @param strOperator - Operator for search.
	 * @return An iterator to the set of results.
	 * @throws DBAppException - Thrown if table does not exist/column does not exist in table.
	 */
	public static Iterator<Row> selectFromTable(Properties properties,
					HashMap<String, Table> tableMap, String strTableName,
					Hashtable<String, String> htblColNameValue, String strOperator)
					throws DBAppException {
		
		if(!tableMap.containsKey(strTableName))
			throw new DBAppException("Table not found for selection.");
		Table t = tableMap.get(strTableName);
		
		if (checkColumnsExist(t, htblColNameValue) == false) {
			throw new DBAppException("Column not found for selection.");
		}

		Iterator<Row> r1 = null;
		Iterator<Row> r2 = null;
		Iterator<Row> res;
		boolean first = false;
		Index index = null;
		
		Set<String> keys = htblColNameValue.keySet();
		for (String key : keys) {
			String val = htblColNameValue.get(key);
			boolean hasIndex = false;
			Iterator<Column> cols = t.getColumns();
			while (cols.hasNext()) {
				Column c = cols.next();
				if (key.equals(c.getTitle())) {
					if (c.isIndex() || c.isKey()) {
						System.out.println("Target: " + key + " Indexed.");
						index = c.getIndex();
						hasIndex = true;
					}
					break;
				}
			}
			
			if (first == false) {
				if (hasIndex == true) {
					r1 = index.search(val);
				}
				else {
					r1 = searchPages(tableMap, strTableName, key, val);
				}
				first = true;
			}
			else {
				if (hasIndex == true) {
					r2 = index.search(val);
				}
				else {
					r2 = searchPages(tableMap, strTableName, key, val);
				}
			}
		}
		
		res = merge(r1,r2, strTableName, strOperator);
		
		return res;
	}
	
	private static boolean checkColumnsExist(Table t,
			Hashtable<String, String> htblColNameValue) {
		
		Set<String> keySet = htblColNameValue.keySet();

		ArrayList<String> columnNames = new ArrayList<String>();
		
		Iterator<Column> colIter = t.getColumns();
		while (colIter.hasNext()) {
			columnNames.add(colIter.next().getTitle());
		}
		
		for(String key : keySet)
			if(!columnNames.contains(key))
				return false;
		return true;
		
	}
		
	public static boolean compare(String data, String colName, String colValue) {
		return Row.parseDataString(data).get(colName).equals(colValue);
	}
	
	public static Iterator<Row> searchPages(HashMap<String, Table> tableMap,
							String strTableName, String colName, String colValue)
			throws DBAppException {
		ArrayList<Row> res = new ArrayList<Row>();
		try {
			int pages = PageManager.getPageCount(strTableName);
			for (int i = 0; i < pages; i++) {
				ArrayList<String> s = PageManager.getPage(strTableName, i);
				int j = 0;
				for(String data : s) {
					if (data.charAt(0) == '*') {
//						System.out.println("Deleted already: " + data);
						j++;
						continue;
					}

					if (compare(data, colName, colValue)) {
						Row row = new Row(strTableName, i, j);
						res.add(row);
//						System.out.println(j + " Search includes: " + data);
					} else {
//						System.out.println(j  + " Search excludes: " + data);
					}
					j++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res.iterator();
	}
	
	public static ArrayList<Row> toArrayRow(Iterator<Row> I) {
		ArrayList<Row> R = new ArrayList<Row>();
		while (I.hasNext()) {
			Row r = I.next();
			R.add(r);
		}
		return R;
	}
	
	public static ArrayList<String> toArraySring(String strTableName, ArrayList<Row> R) {
		ArrayList<String> s = new ArrayList<String>();
		for (int i=0; i<R.size(); i++) {
			try {
				Row r = R.get(i);
				String n = PageManager.getLine(strTableName, r.getPageNumber(), r.getLineNumber());
				s.add(n);
			} catch (InvalidLineNumberException e) {
				e.printStackTrace();
			}
		}
		return s;
	}
	
	public static Iterator<Row> merge(Iterator<Row> r1, Iterator<Row> r2, String strTableName, String strOperator) {
		if (strOperator.equals("") || r2 == null) {
			return r1;
		}
		
		ArrayList<Row> R1 = toArrayRow(r1);
		ArrayList<Row> R2 = toArrayRow(r2);
		
		ArrayList<String> s1 = toArraySring(strTableName, R1);
		ArrayList<String> s2 = toArraySring(strTableName, R2);
		
		if (strOperator.equals("OR")) {
			ArrayList<Row> res = new ArrayList<Row>();
			for (int i=0; i<s1.size(); i++) {
				res.add(R1.get(i));
			}
			boolean flag = false;
			for (int i=0; i<s2.size(); i++) {
				flag = false;
				for (int j=0; j<s1.size(); j++) {
					if (s1.get(j).equals(s2.get(i))) {
						flag = true;
						break;
					}
				}
				if (flag == false) {
					res.add(R2.get(i));
				}
			}
			return res.iterator();
		}
		else {
			ArrayList<Row> res = new ArrayList<Row>();
			boolean flag = false;
			for (int i=0; i<s2.size(); i++) {
				flag = false;
				for (int j=0; j<s1.size(); j++) {
					if (s1.get(j).equals(s2.get(i))) {
						flag = true;
						break;
					}
				}
				if (flag == true) {
					res.add(R2.get(i));
				}
			}
			return res.iterator();
		}
	}
}
