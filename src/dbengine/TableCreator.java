package dbengine;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;

import metadatahandlers.MetadataWriter;
import utilities.Column;
import utilities.PageManager;
import utilities.Table;
import exceptions.DBAppException;

public class TableCreator {
	
	/**
	 * Creates a new table.
	 * @param properties - The target DBApp properties.
	 * @param tableMap - The target DBApp tableMap.
	 * @param strTableName - The table name.
	 * @param htblColNameType - A map of column types.
	 * @param htblColNameRefs - A map of column references.
	 * @param strKeyColName - The name of the key column.
	 * @throws DBAppException - Thrown if table exists or key not found.
	 */
	public static void createTable(Properties properties,
			HashMap<String, Table> tableMap, String strTableName,
			Hashtable<String, String> htblColNameType,
			Hashtable<String, String> htblColNameRefs, String strKeyColName)
			throws DBAppException {

		if (tableMap.containsKey(strTableName))
			throw new DBAppException("(TableCreator:createTable) Error: "
					+ strTableName + " already exists.");
		else if (!validTableName(strTableName))
			throw new DBAppException(
					"(TableCreator:createTable) Error: "
							+ strTableName
							+ " is not an acceptable table name.\n"
							+ "		Allowed characters: Alphabetic/Numerical/Underscore/Dollar(After first)/Hashtag/At\n"
							+ "		Disallowed keywords: null");
		else if (!htblColNameType.containsKey(strKeyColName))
			throw new DBAppException("Key Column not found!");
		
		Table tbl = new Table(strTableName);
		tbl.addColumns(tableMap, htblColNameType, htblColNameRefs,
				strKeyColName);

		tableMap.put(strTableName, tbl);

		Iterator<Column> columnsIterator = tbl.getColumns();

		while (columnsIterator.hasNext()) {
			Column column = columnsIterator.next();

			if (column.isIndex())
				IndexCreator.createIndex(properties, tableMap, strTableName,
						column.getTitle());
			try {
				MetadataWriter.write(column.getMetadata());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		PageManager.createPage(strTableName);
	}

	public static boolean validTableName(String tableName) {
		if (tableName.length() == 0)
			return false;

		Character firstChar = tableName.charAt(0);
		boolean firstOk = Character.isLetterOrDigit(firstChar)
				|| firstChar.equals('_') || firstChar.equals('@')
				|| firstChar.equals('#');
		if (!firstOk)
			return false;

		for (int i = 0; i < tableName.length(); i++) {
			Character charAt = tableName.charAt(i);
			boolean charOk = Character.isLetterOrDigit(charAt)
					|| charAt.equals('_') || charAt.equals('@')
					|| charAt.equals('#') || charAt.equals('$');
			if (!charOk)
				return false;
		}

		if (tableName.equals("null"))
			return false;

		return true;
	}
}
